# Salesforce Ant deployment boilerplate

## Description

### Preparations

Put this project in the folder `tmp` in the project you want to deploy using Ant.

Add `tmp` folder to your `.gitignore` file if you don't want to add this to your project's source code.

Add your salesforce org user credentials to the config file `sf-ant/config/credentials.json` 


#### For Windows users
Install Chocolatey - The Package Manager for Windows
```sh
> Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

Install utils:
```sh
> chocolatey install jq -y
```

#### For Mac users

Install utils:
```sh
> brew install jq -y
```

### Usage

Possible actions:
* `validate` - Validate your code to salesforce org
* `validate_test` - Validate your code to salesforce org and run tests
* `deploy` - Deploy your code to salesforce org
* `deploy_test` - Deploy your code to salesforce org and run tests

`<org_name>` is the name of your salesforce org which credentials you want to use from `sf-ant/config/credentials.json` file. 

From the root of your project run:

```sh
> ./tmp/sf-ant/scripts/main.sh <action> <org_name>

# Example: ./tmp/sf-ant/scripts/main.sh validate uat
```


