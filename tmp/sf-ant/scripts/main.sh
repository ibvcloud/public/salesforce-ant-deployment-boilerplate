#!/usr/bin/env bash
set -e

__SCRIPT_DIR=$(cd "$(dirname "$0")" && pwd)
__PARENT_DIR="$(dirname "$__SCRIPT_DIR")"

__TMP_DIR="$(dirname "$__PARENT_DIR")"
__SRC_DIR="$(dirname "$__TMP_DIR")/src"
__ANT_DIR=${__PARENT_DIR}/ant

__CONFIG_PATH=${__PARENT_DIR}/config/config.json
__CRED_PATH=${__PARENT_DIR}/config/credentials.json



function init(){
    getConfig
    mergeToUAT
}

function getConfig(){
    __RELEASE_NUMBER=`cat $__CONFIG_PATH | jq -r '.release_number'`
    __TO_UAT_FILENAME=`cat $__CONFIG_PATH | jq -r '.tasks_to_full_filename'`

    __QA_ENDPOINTS=(`cat $__CONFIG_PATH | jq -r '.qa_endpoints | .[]'`)
    __QA_ENDPOINT_1=`cat $__CONFIG_PATH | jq -r '.qa_endpoints | .[0]'`
    __QA_ENDPOINT_2=`cat $__CONFIG_PATH | jq -r '.qa_endpoints | .[1]'`
    __PROD_ENDPIONT=`cat $__CONFIG_PATH | jq -r '.prod_endpoint'`
    __TO_UAT_FILE_PATH=$__PARENT_DIR/files/$__TO_UAT_FILENAME

    if [ -z "$__RELEASE_NUMBER" ]; then
        echo "Please specify a release_number in $__CONFIG_PATH"
        exit
    fi
}
function getCredentials(){
    echo "Using $__ENV credentials"
    __maxPoll=`cat $__CRED_PATH | jq -r '.maxPoll'`
    __username=`cat $__CRED_PATH | jq -r '.'$__ENV'.username'`
    __password=`cat $__CRED_PATH | jq -r '.'$__ENV'.password'`
    __serverUrl=`cat $__CRED_PATH | jq -r '.'$__ENV'.serverUrl'`
}

function mergeToUAT(){

    for __BRANCH_NAME in `cat $__TO_UAT_FILE_PATH`
    do
         echo "- $__BRANCH_NAME"
    done
}

function replaceEndpoints() {
    getConfig

    find $__SRC_DIR -type f -print | xargs  sed -i -e s/$__QA_ENDPOINT_1/$__PROD_ENDPIONT/g
    find $__SRC_DIR -type f -print | xargs  sed -i -e s/$__QA_ENDPOINT_2/$__PROD_ENDPIONT/g
}

function validate() {
    __ENV=$1
    getCredentials $__ENV
    echo "Validate to ${__ENV}"

    cd $__ANT_DIR
    ant validate -Dusername=${__username} -Dpassword=${__password} -Dserverurl=${__serverUrl} -DmaxPoll=${__maxPoll} -DdeployRoot=${__SRC_DIR}
}
function validate_test() {
    __ENV=$1
    getCredentials $__ENV
    echo "Validate with tests to ${__ENV}"

    cd $__ANT_DIR
    ant validate_test -Dusername=${__username} -Dpassword=${__password} -Dserverurl=${__serverUrl} -DmaxPoll=${__maxPoll} -DdeployRoot=${__SRC_DIR}
}
function deploy() {
    __ENV=$1
    getCredentials $__ENV
    echo "Deploying to ${__ENV}"

    cd $__ANT_DIR
    ant deploy -Dusername=${__username} -Dpassword=${__password} -Dserverurl=${__serverUrl} -DmaxPoll=${__maxPoll} -DdeployRoot=${__SRC_DIR}
}
function deploy_test() {
    __ENV=$1
    getCredentials $__ENV
    echo "Deploying with tests to ${__ENV}"

    cd $__ANT_DIR
    ant deploy_test -Dusername=${__username} -Dpassword=${__password} -Dserverurl=${__serverUrl} -DmaxPoll=${__maxPoll} -DdeployRoot=${__SRC_DIR}
}


"$@"
